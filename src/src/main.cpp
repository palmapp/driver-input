#include <commons/message_broker.h>
#include <commons/properties_parser.h>
#include <fstream>
#include <sstream>
#include <unio/file.h>

#include <boost/algorithm/string/trim_all.hpp>
#include <boost/application.hpp>
#include <boost/asio.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/dll/runtime_symbol_info.hpp>
#include <boost/filesystem.hpp>
#include <commons/cmd_parser.h>
#include <commons/logging.h>
#include <commons/run_safe.h>
#include <cstdlib>

#include <easylogging++.h>

INITIALIZE_EASYLOGGINGPP

using namespace commons;

struct init_context
{
   boost::filesystem::path input_device_path;
   std::string driver_server_connection_string;
   std::string message_channel;
   std::string message_type;

   std::string before_open_system_cmd;
   std::string on_finish_system_cmd;
   bool trim_input;
   bool finish_on_eof;
   int fixed_input_size;
};

class input_driver
{
 public:
   struct read_handler
   {
      message_broker &driver_server_connection;
      std::shared_ptr<boost::asio::posix::stream_descriptor> descriptor;
      std::vector<char> &buffer;
      init_context &context;

      std::shared_ptr<std::string> data;

      void operator()(const boost::system::error_code &ec, size_t len)
      {
         if (!ec)
         {
            std::string temp{};
            temp.reserve(len);
            std::copy(buffer.begin(), buffer.begin() + len, std::back_inserter(temp));
            auto &buffered_data = *data;

            if (context.fixed_input_size < 0)
            {
               std::size_t last = 0;
               std::size_t pos = 0;
               while ((pos = temp.find('\n', pos)) != std::string::npos)
               {
                  buffered_data.insert(buffered_data.end(), temp.begin() + last,
                                       temp.begin() + pos);

                  if (context.trim_input)
                  {
                     boost::trim(buffered_data);
                  }
                  LOG(DEBUG) << "line input: \"" << buffered_data << "\"";

                  driver_server_connection.publish_message(context.message_channel,
                                                           context.message_type, buffered_data);
                  buffered_data.clear();
                  last = ++pos;
               }

               buffered_data.insert(buffered_data.end(), temp.begin() + last, temp.end());
            }
            else
            {
               if (context.trim_input)
               {
                  boost::trim(temp);
               }

               buffered_data += temp;

               if ((!buffered_data.empty() && context.fixed_input_size == 0) ||
                   buffered_data.size() >= context.fixed_input_size)
               {
                  LOG(DEBUG) << "input: \"" << buffered_data << "\"";
                  driver_server_connection.publish_message(context.message_channel,
                                                           context.message_type, buffered_data);
                  buffered_data.clear();
               }
            }

            descriptor->async_read_some(boost::asio::buffer(buffer.data(), buffer.size()), *this);
         }
         else
         {
            LOG(WARNING) << "read operation failed: " << ec.message();
         }
      }
   };

   input_driver(boost::application::context &context) : context_(context) {}

   void work()
   {
      using namespace unio;
      auto status = context_.find<boost::application::status>();
      auto config = *context_.find<init_context>();
      std::vector<char> buffer;
      buffer.resize(4096);

      while (*status.get() != boost::application::status::stopped)
      {

         bool result = run_safe([&, this] {
            message_broker driver_server_connection{message_broker_type::driver_client,
                                                    config.driver_server_connection_string};

            if (!config.before_open_system_cmd.empty())
            {
               int r = std::system(config.before_open_system_cmd.c_str());
               LOG(INFO) << "executing cmd: " << config.before_open_system_cmd
                         << " with result: " << r;
            }

            unio::file input_device_path(config.input_device_path,
                                         file::read_write | file::binary | file::sequential_access);
            driver_server_connection.publish_message(config.message_channel, "started",
                                                     config.input_device_path.string());

            while (*status.get() != boost::application::status::stopped)
            {
               {
                  std::lock_guard<std::mutex> lock{mutex_};
                  current_op_ = std::make_shared<boost::asio::posix::stream_descriptor>(
                      io_, input_device_path.release());
               }
               read_handler handler{driver_server_connection, current_op_, buffer, config,
                                    std::make_shared<std::string>()};

               current_op_->non_blocking(true);
               current_op_->async_read_some(boost::asio::buffer(buffer.data(), buffer.size()),
                                            handler);

               io_.run();

               LOG(INFO) << "input driver read finished";

               if (config.finish_on_eof)
               {
                  break;
               }
            }

            const char *reason = "closed";
            if (*status.get() != boost::application::status::stopped)
            {
               driver_server_connection.publish_message(config.message_channel, "ended", "failure");
            }
            else
            {
               driver_server_connection.publish_message(config.message_channel, "ended", "closed");
            }

            if (!config.on_finish_system_cmd.empty())
            {
               int r = std::system(config.on_finish_system_cmd.c_str());
               LOG(INFO) << "executing cmd: " << config.on_finish_system_cmd
                         << " with result: " << r;
            }

            std::this_thread::sleep_for(std::chrono::seconds{1});
            driver_server_connection.close_and_discard_unsent_messages();
         });

         if (!result)
         {
            std::this_thread::sleep_for(std::chrono::seconds{5});
         }
      }
   }

   int operator()()
   {
      thread_ = std::thread(&input_driver::work, this);
      context_.find<boost::application::wait_for_termination_request>()->wait();

      thread_.join();

      return 0;
   }

   bool stop()
   {
      LOG(INFO) << "stop requested";
      {
         std::lock_guard<std::mutex> lock{mutex_};
         current_op_->close();
      }
      io_.stop();
      return true; // return true to stop, false to ignore
   }

   // windows specific (ignored on posix)
   bool pause()
   {
      return true; // return true to pause, false to ignore
   }

   bool resume()
   {
      return true; // return true to resume, false to ignore
   }

 private:
   boost::application::context &context_;
   std::thread thread_;
   boost::asio::io_service io_;

   std::mutex mutex_;
   std::shared_ptr<boost::asio::posix::stream_descriptor> current_op_;
};

auto inline add_prefix_key(const std::string &prefix, const std::string &key)
{
   std::stringstream ss;
   ss << "driver.";
   ss << prefix;
   ss << ".";
   ss << key;
   return ss.str();
}

auto inline add_prefix_value(const std::string &prefix, const std::string &value)
{
   std::stringstream ss;
   ss << value;
   ss << "/";
   ss << prefix;
   return ss.str();
}

int main(int argc, const char *argv[])
{
   boost::filesystem::path config;
   std::string prefix;
   if (cmd::try_get_arg(config, "-c", argc, argv) && cmd::try_get_arg(prefix, "-p", argc, argv))
   {
      auto base_path = boost::dll::program_location().parent_path();
      cmd::try_get_arg(base_path, "-b", argc, argv);

      std::string input = "kiosek-input-";
      input += prefix;
      commons::init_easylogging(argc, argv, input.c_str(), base_path);

      std::ifstream cfg_file{config.c_str()};
      auto properties = parse_properties_to_map(cfg_file);

      auto input_device_path = get_map_value(properties, add_prefix_key(prefix, "device.path"),
                                             add_prefix_value(prefix, "/opt"));
      auto driver_manager_connection_string =
          get_map_value(properties, "driver.server.connection", "tcp://127.0.0.1:54321");
      auto message_channel =
          get_map_value(properties, add_prefix_key(prefix, "device.message.channel"),
                        add_prefix_value(prefix, "system"));
      auto message_type =
          get_map_value(properties, add_prefix_key(prefix, "device.message.type"), prefix);
      auto before_open =
          get_map_value(properties, add_prefix_key(prefix, "device.before.open.cmd"), "");
      auto on_finish =
          get_map_value(properties, add_prefix_key(prefix, "device.after.close.cmd"), "");
      auto trim = get_map_value(properties, add_prefix_key(prefix, "device.message.trim"),
                                "true") == "true";
      auto finish_on_eof = get_map_value(properties, add_prefix_key(prefix, "device.message.eof"),
                                         "false") == "true";
      auto fixed_input_size =
          get_map_value(properties, add_prefix_key(prefix, "device.fixed.input.size"), "");

      LOG(INFO) << "starting input: " << input_device_path
                << "\nserver: " << driver_manager_connection_string
                << "\nmessage channel: " << message_channel << "\nmessage type: " << message_type
                << "\nbefore open cmd: " << before_open << "\ntrim: " << trim
                << "\nfinish on eof: " << finish_on_eof
                << "\n fixed input size: " << fixed_input_size;

      int fixed_input_size_bytes = 0;
      if (!fixed_input_size.empty())
      {
         fixed_input_size_bytes = std::atoi(fixed_input_size.c_str());
      }

      boost::application::context ctx;
      ctx.insert(boost::application::csbl::make_shared<init_context>(init_context{
          input_device_path, driver_manager_connection_string, message_channel, message_type,
          before_open, on_finish, trim, finish_on_eof, fixed_input_size_bytes}));

      boost::system::error_code ec;
      boost::application::auto_handler<input_driver> app(ctx);
      auto res = boost::application::launch<boost::application::common>(app, ctx, ec);
      if (ec)
      {
         LOG(ERROR) << "result error code: " << ec.value() << " => " << ec.message();
      }

      return res;
   }
   else
   {
      LOG(ERROR)
          << "you need to provide configuration file path by -c option and prefix by -p option";
      return 1;
   }
}
